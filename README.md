# In-Circuit Capacitor Tester

This design is by Rue Mohr. I simply took their schematic and turned it into a KiCAD
project and made a simple PCB layout.

The way this works is you connect the output to both the cap under test and your
oscilloscope. Depending on how the waveform looks, you can quickly see if a capacitor
is leaky (bad ESR) or still functioning well.